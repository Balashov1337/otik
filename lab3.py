def write_header_balash(file_name):
    dot = file_name.find('.')
    if dot != -1:
        new_file_name = file_name[:dot + 1] + 'balash'
    else:
        new_file_name = file_name + '.balash'

    try:
        new_file = open(new_file_name, "wb")
        # write signature
        new_file.write("azuz".encode("utf-8"))
        # write version
        new_file.write("00".encode("utf-8"))
        # write entropy
        new_file.write((0).to_bytes(1, 'big'))
        # write compression
        new_file.write((0).to_bytes(1, 'big'))
        # write noise protection
        new_file.write((0).to_bytes(1, 'big'))
        # write aligment
        new_file.write((0).to_bytes(7, 'big'))
        new_file.close()
    except Exception as e:
        print(e)
    return new_file_name


def balash_test(file_name):
    try:
        binfile = open(file_name, "rb")

        # check signature
        fsignature = binfile.read(4)
        if fsignature.decode("utf-8") != "azuz":
            print("Signature no 'azuz'!!!")
            return 0
        # check version
        version = binfile.read(2)
        if version.decode("utf-8") != "00":
            print("Version miss!!!")
            return 0
        # check entropy
        buf = binfile.read(1)
        buf = int.from_bytes(buf, 'big')
        if buf != 0:
            print("Wrong Entropy!!!")
            return 0
        # check compression
        buf = binfile.read(1)
        buf = int.from_bytes(buf, 'big')
        if buf != 0:
            print("Wrong compression!!!")
            return 0
        # check noise protection
        buf = binfile.read(1)
        buf = int.from_bytes(buf, 'big')
        if buf != 0:
            print("Wrong noise shield!!")
            return 0
        # check aligment
        buf = binfile.read(7)
        buf = int.from_bytes(buf, 'big')
        if buf != 0:
            print("Wrong alg!!")
            return 0
        print("Ne nu po lubomu mi codirovali")
    except Exception as e:
        print(e)
    return 1


def balash_preview(file_name):
    dot = file_name.find('.')
    print(file_name[:dot])


def block_encode(file_name, new_file_name):
    try:
        with open(file_name, "rb") as file:
            old = file.read()

        if old[0:4].decode("utf-8") == "azuz":
            print("Вы хотите повторно зархивировать архив .azuz!\nНе надо так")
            return

        new_file = open(new_file_name, "ab")

        start = 0
        end = 15
        while len(old[start:end]) > 16:
            new_file.write((16).to_bytes(1, 'big'))
            new_file.write(old[start:end])
            start += 16
            end += 16

        if len(old) < 16:
            new_file.write(len(old[start:]).to_bytes(1, 'big'))
            new_file.write(old[start:])

        new_file.close()
        print("Encode file: ", new_file_name)
    except Exception as e:
        print(e)


def balash_encode(file_name):
    new_file = write_header_balash(file_name)
    block_encode(file_name, new_file)


def block_decode(file_name):
    try:
        with open(file_name, "rb") as file:
            data = file.read()

        if data[0:4].decode("utf-8") != "azuz":
            print("Это не архив .azuz!\nТакое я не разархивирую :с")
            return

        dot = file_name.find('.')
        dec_file_name = "dec" + file_name[:dot]
        decfile = open(dec_file_name, "wb")

        start = 16
        end = 17
        sizeblock = int.from_bytes(data[start:end], 'big')
        while sizeblock:
            start += 1
            end += sizeblock
            decfile.write(data[start:end])
            start = end + 1
            end += 1
            sizeblock = int.from_bytes(data[start:end], 'big')

        decfile.close()
        print("Decode file is ", dec_file_name)
    except Exception as e:
        print(e)


def balash_decode(file_name):
    block_decode(file_name)


def do_op(mode, file_name: str):
    if mode == "enc":
        balash_encode(file_name)
    elif mode == "dec":
        balash_decode(file_name)
    elif mode == "test":
        balash_test(file_name)
    elif mode == "view":
        balash_preview(file_name)


def balash():
    modes = ["enc", "dec", "test", "view"]
    file_name = str(input("Enter file name"))

    print("Choise you destini: ", modes)
    mode = str(input("Enter mode: (example: enc)"))
    while mode != modes[0] and mode != modes[1] and mode != modes[2] and mode != modes[3]:
        print("you must choise 1 on mode")
        print("mode: ", modes)
        mode = str(input("Enter mode:"))
    do_op(mode, file_name)


if __name__ == "__main__":
    balash()

