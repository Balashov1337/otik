import struct
import os
import sys


def decoder(name, out, search):
    MAX_SEARCH = search
    file = open(name, "rb")
    input = file.read()

    chararray = ""
    i = 0

    while i < len(input):
        (offset_and_length, char) = struct.unpack(">HB", input[i:i + 3])
        #print(offset_and_length,char)
        offset = offset_and_length >> 6
        length = offset_and_length - (offset << 6)
        i = i + 3
        if (offset == 0) and (length == 0):
            chararray += chr(char)
        else:
            iterator = len(chararray) - MAX_SEARCH
            if iterator < 0:
                iterator = offset
            else:
                iterator += offset
            for pointer in range(length):
                chararray += chararray[iterator + pointer]
            chararray += chr(char)
        #print(chararray)
    out.write(chararray)


def main():
    MAX_SEARCH = int(sys.argv[1])
    file_type = sys.argv[2]
    processed = open("processed." + file_type, "w")
    decoder("res.bin", processed, MAX_SEARCH)
    processed.close


if __name__ == "__main__":
    main()