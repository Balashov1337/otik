import huffman
import pickle
import re
import os
import pandas as pd

class Huffman:
    def Save(self, path):

        pass


    def Code(self, source,target):
        # get source
        out = {}
        text = open(source, mode='rb').read()

        out['name'] = re.search(pattern=r'[^\\]+\.\w+', string=source).group()
        out['size'] = len(text)

        # get dict
        dic = huffman.codebook(
            pd.DataFrame({'value': bytearray(text)}).
                groupby('value').value.
                count().items())

        # get reversed dict for decoding
        out['dict'] = dict(zip(dic.values(), dic.keys()))

        # get huffman code
        data = ''.join([dic[ch] for ch in text])

        #get bytes
        data = re.findall(pattern=r'\d{1,8}', string=data)
        while len(data[-1]) < 8:
            data[-1] += '0'
        print(data)

        out['data'] = bytes([int(b, 2) for b in data])

        # write to file
        with open(target, 'wb') as f:
            pickle.dump(out, f)

    def Decode(self,source, target):
        file = open(source, 'rb')
        #print(len(file.read()))
        file = pickle.load(file)

        huf_code = [bin(ch)[2:] for ch in file['data']]

        for i in range(len(huf_code)):
            while(len(huf_code[i]) < 8):
                huf_code[i] = '0' + huf_code[i]

        huf_code = ''.join(huf_code)
        key = ''
        decoded = []

        for bit in huf_code:
            key += bit
            if key in file['dict']:
                decoded += [file['dict'][key]]
                key = ''
            if len(decoded) == file['size']:
                break

        decoded = bytes(decoded)

        open(target, mode='wb').write(decoded)




source = r"C:\Users\GrAf\Desktop\Толстой Лев. Война и мир. Том 1 и 2 - royallib.com.txt"
coded = r"C:\Users\GrAf\Desktop\coded.myarh"
decoded = r"C:\Users\GrAf\Desktop\decoded.txt"
h = Huffman()
h.Code(source, coded)
h.Decode(coded, decoded)

